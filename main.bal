import ballerina/http;
import ballerina/uuid;
import ballerina/io;


isolated map<json> Learners = {};

isolated function getLearnerById(string learnerId) returns json {
    lock {
        return Learners[learnerId].clone();
    }
}

isolated function getAllLearner() returns json {
    lock {
        return Learners.clone();
    }
}

isolated function createLearner(string LearnerId, json learnerReq) {
    lock {
        Learners[LearnerId] = learnerReq.clone();
    }
}

isolated function UpdateLearner(string LearnerId, json learnerReq) {
    lock {
        Learners[LearnerId] = learnerReq.clone();
    }
}

isolated function getLearnerMaterials() returns json {
    lock {
        return Materials.clone();
    }
}

service /learner on new http:Listener(9090) {

    resource function get findLearnerById(http:Caller caller, http:Request req, string learnerId) {
        json payload = getLearnerById(learnerId);
        http:Response response = new;
        if (payload == null) {
            payload = "Learner : " + learnerId + " cannot be found.";
        }

        response.setJsonPayload(payload);

        var result = caller->respond(response);
        if (result is error) {
            io:println("Error sending response", result);
        }
    }

    resource function get findAllLearner(http:Caller caller, http:Request req, string learnerId) {
        json? payload = getAllLearner();
        http:Response response = new;
        if (payload == null) {
            payload = "No Learners found";
        }

        response.setJsonPayload(payload);

        var result = caller->respond(response);
        if (result is error) {
            io:println("Error sending response", result);
        }
    }

    @http:ResourceConfig {
        consumes: ["application/json"]
    }
    resource function post create(http:Caller caller, http:Request request) {
        http:Response response = new;
        var learnerReq = request.getJsonPayload();

        if (learnerReq is json) {

            string LearnerId = uuid:createType1AsString();
            createLearner(LearnerId, learnerReq);

            json payload = {status: "Learner Created.", learnerId: LearnerId};
            response.setJsonPayload(payload);

            response.statusCode = 201;

            var result = caller->respond(response);
            if (result is error) {
                io:println("Error in responding: ", result);
            }

        } else {

            response.statusCode = 400;
            response.setPayload("Invalid payload received");
            var result = caller->respond(response);
            if (result is error) {
                io:println("Error in responding: ", result);
            }
        }

    }

    resource function put update(http:Caller caller, http:Request request, string learnerId) returns error? {
        var updatedLearner = request.getJsonPayload();
        http:Response response = new;

        if (updatedLearner is json) {

            json existingLearner = getLearnerById(learnerId);
            Learner CastUpdateLearner = check updatedLearner.cloneWithType(Learner);
            Learner CastExistLearner = check getLearnerById(learnerId).cloneWithType(Learner);

            if (existingLearner != null) {
                CastExistLearner.username = CastUpdateLearner.username;
                CastExistLearner.lastname = CastUpdateLearner.lastname;
                CastExistLearner.firstname = CastUpdateLearner.firstname;
                CastExistLearner.preferred_formats = CastUpdateLearner.preferred_formats;
                CastExistLearner.past_subjects = CastUpdateLearner.past_subjects;
                UpdateLearner(learnerId, CastExistLearner);
            } else {
                existingLearner = "Learner : " + learnerId + " cannot be found.";
            }

            response.setJsonPayload(updatedLearner);
            var result = caller->respond(response);
            if (result is error) {
                io:print("Error sending response", result);
            }
        } 
        else {
            response.statusCode = 400;
            response.setPayload("Invalid payload received");
            var result = caller->respond(response);
            if (result is error) {
                io:print("Error sending response", result);
            }
        }

    }

    resource function get learningMaterials() returns json {
        return getLearnerMaterials();
    }
}


type Subjects record {|
    string course;
    string score;
|};

type Learner record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subjects[] past_subjects;
|};

type LearningMaterials record {|
    string course;
    LearningObjects learning_objects;
|};

type LearningObjects record {|
    any required;
    any suggested;
|};

type Details record {|
    string name;
    string Description;
    string difficulty;
|};

isolated  final json Materials = 
{
    "course": "Distributed Systems Applications", 
        "learning_objects": {
            "required": {
                "audio": [
                    {
                        "name": "gRPC", 
                        "description": "performance Remote Procedure Call (RPC) framework that can run in any environment.", 
                        "difficulty": "Easy"
                    }
                ], 
                "text": [
                    {}
                ]
            }, 
            "suggested": {
                "video": [], 
                "audio": []
            }
        }
};
